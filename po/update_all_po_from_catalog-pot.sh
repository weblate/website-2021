#!/bin/bash
# Extract catalog pot from the website sources:

echo "Update all PO files from _catalog.pot"

# Get to location of root of the website:
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd ${SCRIPT_DIR}
cd ..

POT_REFERENCE=${SCRIPT_DIR}/_catalog.pot

#restore directory position
cd ${SCRIPT_DIR}

# update all POs
for PO_FILE in ${SCRIPT_DIR}/*.po; do
  if [ -f "${PO_FILE}" ] ; then
    echo "==============================="
    echo "Merging $(basename $PO_FILE)"
    # Sort by file occurrence (this order will give more context to translators)
    # Fuzzy matching is very noisy, so skipping that
    msgmerge -UFNv ${PO_FILE} ${POT_REFERENCE}
    # Unclutter - we don't need the backups
    rm -f ${PO_FILE}~
  fi
done

echo "==============================="
echo "Done."
