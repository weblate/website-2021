#!/bin/bash
# Extract translation percentage completion:
# Inspired by https://github.com/Ichiniro/dotfiles_kde/blob/master/.local/share/plasma/plasmoids/org.kpple.kppleMenu/translate/merge.sh

echo "Extract translation percentage completion"

# Get to location of root of the website:
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
POT_REFERENCE=${SCRIPT_DIR}/_catalog.pot
TARGET_DIR=../locale/en_US.utf8/LC_MESSAGES/
OUTPUT_JSON_FILE=${TARGET_DIR}/all-po-translation-percentage.json

# Extracting all number of string to translate
POT_ALL_MESSAGE_COUNT=`expr $(grep -Pzo 'msgstr ""\n(\n|$)' $POT_REFERENCE | grep -c 'msgstr ""')`

#restore directory position
cd ${SCRIPT_DIR}

# Json header
echo '{' > $OUTPUT_JSON_FILE

# update all POs
for PO_FILE in ${SCRIPT_DIR}/*.po; do
  if [ -f "${PO_FILE}" ] ; then

    echo "==============================="
    # Sort by file occurrence (this order will give more context to translators)
    PO_FILENAME=$(basename $PO_FILE)
    PO_TEMPORARY_FILE=$PO_FILENAME"~"
    LANG="${PO_FILENAME%%.*}"

    # Fuzzy matching is very noisy, so skipping that
    msgmerge -FNq ${PO_FILE} ${POT_REFERENCE} -o $PO_TEMPORARY_FILE

    # Extract percentage
    PO_EMPTY_MESSAGE_COUNT=`expr $(grep -Pzo 'msgstr ""\n(\n|$)' $PO_TEMPORARY_FILE | grep -c 'msgstr ""')`
    PO_MESSAGE_DONE=`expr $POT_ALL_MESSAGE_COUNT - $PO_EMPTY_MESSAGE_COUNT`
    PO_PERCENTAGE=$((100 * $PO_MESSAGE_DONE/$POT_ALL_MESSAGE_COUNT ))

    # Display
    echo "$LANG : ${PO_PERCENTAGE} %"

    # Write Json entry
    JSON_ENTRY='"'$LANG'": '${PO_PERCENTAGE}','
    echo $JSON_ENTRY >> $OUTPUT_JSON_FILE

    # Unclutter - we don't need the backups
    rm -f ${PO_FILE}~
  fi
done

# Json footer
echo '}' >> $OUTPUT_JSON_FILE

echo "==============================="
echo "Done."

# Display Json for debug
#cat $OUTPUT_JSON_FILE
