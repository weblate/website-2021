# How to translate the website

The translation consists of 3 parts:

1. A `<locale>-credits.json` file listing the translators for your language
2. A `<locale>.po` file with the actual translation source code.
   This will be compiled by a script into the `locale` directory.
3. Optionally, a `<locale>.svg` file with the header image

For further guidance, please refer to the [documentation on the website](https://www.peppercarrot.com/en/documentation/110_Translate_the_website.html).

# How to maintain the translations

This directory has 3 scripts in it for maintaining the translations:

1. `extract_strings_from_PHP_and_save_to_catalog-pot.sh`: Updates the `_catalog.pot` file.
   Run this whenever there's a change in the website's source code that changes a translatable string.
2. `update_all_po_from_catalog-pot.sh`: Updates all `<locale>.po` files with the
   information from `_catalog.pot`, so that translators can start working.
   Note that every tool uses different line breaks in this file format,
   so be careful about not creating merge conflicts.
3. `compile_all_PO_into_MO.sh`: Turns `<locale>.po` translation source files into
   binary `.mo` files and places them under the `locale` directory.
   Run this script on the live installation whenever there's a translation update.
   Do not commit the `.mo` files into git.
