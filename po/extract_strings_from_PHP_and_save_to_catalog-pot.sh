#!/bin/bash
# Extract catalog pot from the website sources:

echo "Updating translation reference catalog"

# Get to location of root of the website:
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd ${SCRIPT_DIR}
cd ..

POT_REFERENCE=${SCRIPT_DIR}/_catalog.pot

echo "==============================="

# Extract php from index at root:
xgettext --from-code=UTF-8 --add-comments --language="PHP" -o ${POT_REFERENCE} *.php
# Join to it (-j) all php from core:
cd core/
xgettext --from-code=UTF-8 --add-comments --language="PHP" -j -o ${POT_REFERENCE} *.php

#restore directory position
cd ${SCRIPT_DIR}

echo "==============================="
echo "Done."
