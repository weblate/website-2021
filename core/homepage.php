<?php if ($root=="") exit;

echo '<div class="container">'."\n";

# Include the language selection menu
include($file_root.'core/mod-menu-lang.php');

echo '  <section class="col sml-12">'."\n";

echo '    <div class="cover col sml-12">'."\n";
echo '      <div class="covertextoverlay">'."\n";

  echo '        <img class="dr" src="'.$root.'/core/img/David-Revoy_logo.svg" alt="David Revoy" title="David Revoy" />';

if (file_exists('po/'.$lang.'.svg')) {
  echo '        <img class="logo" src="'.$root.'/po/'.$lang.'.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
} else {
  echo '        <img class="logo" src="'.$root.'/po/en.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
}

echo '          <h1>'."\n";
echo '            '._("A free(libre) and open-source webcomic supported directly by its patrons to change the comic book industry!").''."\n";
echo '          </h1>'."\n";
echo '          <a class="moreinfobutton" style="background-color: #288ba2" href="'.$root.'/'.$lang.'/webcomics/index.html" title="'._("Read the webcomic for free").'">'."\n";
echo '            '._("Read the webcomic for free").''."\n";
echo '          </a>'."\n";
echo '          <a class="moreinfobutton" href="'.$root.'/'.$lang.'/support/index.html"" title="'._("Become a patron").'">'."\n";
echo '            '._("Become a patron").''."\n";
echo '          </a>'."\n";
echo '        </div>'."\n";
echo '      </div>'."\n";
echo ''."\n";

echo '  </section>'."\n";
echo '  <section class="col sml-12 med-6 lrg-4" style="margin-bottom:3rem;">'."\n";
echo '    <h3 style="margin-top: 0; margin-bottom: 0.2rem; display: block; text-align: center; font-size: 1.6rem;">'._("Latest episode").'</h3>'."\n";
echo ''."\n";

# Array of all episodes (copied from database to sort it backward, newer episode on top)
$all_episodes = $episodes_list;
rsort($all_episodes);

$all_episodes_count = count($all_episodes);
$episodes_homepage = array();
$episodes_homepage = array_slice($all_episodes, 0, 1);


# Display latest episode
# ----------------------
foreach ($episodes_homepage as $key => $episode_directory) {
  $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
  $cover_path = ''.$sources.'/'.$episode_directory.'/low-res/'.$lang.''.$credits.'E'.$episode_number.'.jpg';
  $episode_link = $root.'/'.$lang.'/webcomic/'.$episode_directory.'.html';
  # In case the cover is not available in the current language, fallback to English.
  $cover_description = ''._("Click to read episode $episode_number.").'';
  if (!file_exists($cover_path)) {
    $cover_path = ''.$sources.'/'.$episode_directory.'/hi-res/en'.$credits.'E'.$episode_number.'.jpg';
  }
  echo '    <figure class="thumbnail col sml-12">'."\n";
  echo '      <a href="'.$episode_link.'">'."\n";
  echo '        ';
  _img($root.'/'.$cover_path, $cover_description, 480, 399, 89);
  echo ''."\n";
  echo '      </a>'."\n";
  echo '    </figure>'."\n";
}
echo ''."\n";

echo '    <a class="loadmorebutton" href="'.$root.'/'.$lang.'/webcomics/index.html">'.sprintf(ngettext('Show %d episode', 'Show all %d episodes', $all_episodes_count), $all_episodes_count).'</a>'."\n";
echo '  </section>'."\n";
echo ''."\n";

# Display Recent News
# -------------------
echo '  <section class="col sml-12 med-6 lrg-4"  style="margin-bottom:3rem;">'."\n";
echo '      <h3 style="margin-top: 0; margin-bottom: 0.2rem; display: block; text-align: center; font-size: 1.6rem;">'."\n";
echo '      <a style="text-decoration: none;" href="https://www.davidrevoy.com/feed/en/rss" target="_blank" title="Rss">'."\n";
echo '        <img style="border:none; padding: 0 0; margin: 0 0; display: inline-block;" width="20px" height="20px" src="'.$root.'/core/img/s_rss.svg" alt="Rss"/>'."\n";
echo '      </a>'._("Recent blog-posts").' </h3>'."\n";
echo '      <ul class="page" style="display: block; height: 333px; padding: 0 6px; margin: 0 0 8px 0; font-size: 16px; list-style-type: none; line-height: 19px">'."\n";
# Mini RSS reader cached
$rss_url = "https://www.davidrevoy.com/feed/rss";
$cache_filename = $cache.'/_blog-rss.txt';
$cache_limit_in_mins = 60;

# Workaround in case of missing cache file
if (!file_exists($cache_filename)) {
  $cache_placeholder = fopen($cache_filename,"w");
  fwrite($cache_placeholder,"Rss cache was missing, rebuilding it... be patient.");
  fclose($cache_placeholder);
  chmod($cache_filename, 0620);
}

if (file_exists($cache_filename)) {
  $secs_in_min = 60;
  $diff_in_secs = (time() - ($secs_in_min * $cache_limit_in_mins)) - filemtime($cache_filename);
  # check if the cached file is older than our limit
  if ( $diff_in_secs < 0 ) {
    # It isn't, so display it
    $list = file_get_contents($cache_filename);
    # display inner
    echo $list;
  } else {
    # Cache is older now
    # Generate the RSS reader cache for the blog and display it
    $rss_feeds = simplexml_load_file($rss_url);
    $feed_count=0;
    $output_to_cache = array();
    if(!empty($rss_feeds)){
      foreach ($rss_feeds->channel->item as $item) {
        # Read the many information
        $title = $item->title;
        $description = $item->description;
        $link = $item->link;
        $date = $item->pubDate;
        $formated_date = date('d F Y',strtotime($date));

      if($feed_count>=8) break;
        $output_to_cache[] = '  <li style="padding: 10px 32px 10px 32px; overflow: hidden; text-overflow: ellipsis;">'."\n";
        $output_to_cache[] = '    <span style="white-space:nowrap; ">'.$formated_date.' : <a href="'.$link.'" title="'.$title.'">'.$title.'</a></span>'."\n";
        $output_to_cache[] = '  </li>'."\n";
        $feed_count++;
      }
    }

    $file = fopen ( $cache_filename, 'w' );
    fwrite ( $file, implode('',$output_to_cache));
    fclose ( $file );
    chmod($cache_filename, 0620);

    $list = file_get_contents($cache_filename);
    echo $list;
  }
}

echo '      </ul>'."\n";
echo '     <span style="color: silver">'._("(Note: in English only)").'</span>'."\n";
echo '    <a class="loadmorebutton" href="https://www.davidrevoy.com/blog">'._("Go to the blog").'</a>'."\n";
echo '  </section>'."\n";

# Display support box
# -------------------

echo '  <section class="col sml-12 med-6 lrg-4"  style="margin-bottom:3rem;">'."\n";
echo '    <h3 style="margin-top: 0; margin-bottom: 0.2rem; display: block; text-align: center; font-size: 1.6rem;">'._("Support Pepper&Carrot on").'</h3>'."\n";
echo '    <div><figure class="thumbnail col sml-12">'."\n";
echo '      <a href="https://www.patreon.com/join/davidrevoy?" title="Patreon">'."\n";
echo '        <img src="'.$root.'/'.$sources.'/0ther/website/hi-res/2021-07-12_support-us_by-David-Revoy.jpg"/>'."\n";
echo '      </a>'."\n";
echo '    </figure></div>'."\n";
echo '    <a class="loadmorebutton" href="'.$root.'/'.$lang.'/support/index.html">'._("More options").'</a>'."\n";
echo '  </section>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '</div>'."\n";
?>

