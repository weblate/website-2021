<?php if ($root=="") exit; 


function _social_buttons(){
global $root;
echo '  '._("Follow Pepper&Carrot on:").''."\n";
echo '  <div class="social">'."\n";
echo '    <a href="https://framapiaf.org/@davidrevoy" target="_blank" title="Mastodon">'."\n";
echo '      <img width="40px" height="40px" src="'.$root.'/core/img/s_masto.svg" alt="Masto"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="http://twitter.com/davidrevoy" target="_blank" title="Twitter">'."\n";
echo '      <img width="40px" height="40px" src="'.$root.'/core/img/s_tw.svg" alt="Twit"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="https://www.facebook.com/pages/Pepper-Carrot/307677876068903" target="_blank" title="Facebook">'."\n";
echo '      <img width="40px" height="40px" src="'.$root.'/core/img/s_fb.svg" alt="Fbook"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="https://www.instagram.com/deevadrevoy/" target="_blank" title="Instagram">'."\n";
echo '      <img width="40px" height="40px" src="'.$root.'/core/img/s_insta.svg" alt="Insta"/>'."\n";
echo '    </a>'."\n";
echo '    <a href="https://www.davidrevoy.com/feed/en/rss" target="_blank" title="Rss">'."\n";
echo '      <img width="40px" height="40px" src="'.$root.'/core/img/s_rss.svg" alt="Rss"/>'."\n";
echo '    </a>'."\n";
echo '    <br/>'."\n";
echo '    <br/>'."\n";
echo '  '._("Join community chat rooms:").'<br/>'."\n";
echo '    <a href="https://libera.chat/">'."\n";
echo '       '._("IRC: #pepper&carrot on libera.chat").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://matrix.to/#/#peppercarrot:matrix.org">'."\n";
echo '       Matrix'."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://telegram.me/joinchat/BLVsYz_DIz-S-TJZB9XW7A">'."\n";
echo '       Telegram'."\n";
echo '    </a><br/>'."\n";
echo '    <br/>'."\n";
echo '  </div>'."\n";
}


echo '<div style="clear:both;"></div>'."\n";

echo '<footer id="footer">';

echo '  <div class="container" style="max-width:1100px; margin-top: 1.4rem; text-align: center;">';

echo '  <section class="col sml-12 med-hide">'."\n";
_social_buttons();
echo '    <br/>'."\n";
echo '  </section>'."\n";

echo '  <section class="col sml-12 med-4">'."\n";
echo '    <a href="https://www.patreon.com/join/davidrevoy?">'."\n";
echo '       Patreon'."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.tipeee.com/pepper-carrot">'."\n";
echo '       Tipeee'."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://liberapay.com/davidrevoy/">'."\n";
echo '       Liberapay'."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://paypal.me/davidrevoy">'."\n";
echo '       Paypal'."\n";
echo '    </a><br/><br/>'."\n";
echo '    <a href="https://framagit.org/peppercarrot">'."\n";
echo '       Framagit'."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/wiki/">'."\n";
echo '       '._("Wiki").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/tag/making-of">'."\n";
echo '       '._("Making-of").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/tag/brush">'."\n";
echo '       '._("Brushes").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/wallpapers/index.html">'."\n";
echo '       '._("Wallpapers").''."\n";
echo '    </a><br/>'."\n";
echo '  </section>'."\n";

echo '  <section class="col sml-12 med-4">'."\n";
echo '    <a href="'.$root.'/'.$lang.'/">'."\n";
echo '       '._("Homepage").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/webcomics/index.html">'."\n";
echo '       '._("Webcomics").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/artworks/artworks.html">'."\n";
echo '       '._("Artworks").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/goodies/index.html">'."\n";
echo '       '._("Goodies").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/contribute/index.html">'."\n";
echo '       '._("Contribute").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/shop">'."\n";
echo '       '._("Shop").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="https://www.davidrevoy.com/blog">'."\n";
echo '       '._("Blog").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/about/index.html">'."\n";
echo '       '._("About").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/license/index.html">'."\n";
echo '       '._("License").''."\n";
echo '    </a><br/><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/tos/index.html">'."\n";
echo '       '._("Terms of Services and Privacy").''."\n";
echo '    </a><br/>'."\n";
echo '    <a href="'.$root.'/'.$lang.'/documentation/409_Code_of_Conduct.html">'."\n";
echo '       '._("Code of Conduct").''."\n";
echo '    </a><br/>'."\n";
echo '  </section>'."\n";

echo '  <section class="col sml-6 med-4 sml-hide med-show">'."\n";
_social_buttons();
echo '  </section>'."\n";

echo '<div style="clear:both;"></div>'."\n";
echo '<br/>'."\n";
echo '<br/>'."\n";

echo '</footer>';

echo '</body>'."\n";
echo '</html>'."\n";
?>
