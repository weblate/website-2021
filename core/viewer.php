<?php if ($root=="") exit;

# $pattern , $attribution and $title are defined in index.php at root
# This way, the title of the html page can be also defined and a
# thumbnail of the viewer can be generated for social-medias.

# Main container black, overlay even top-bar.
echo '  <div id="viewer">'."\n";

# Extract the date
$picture_date = str_replace('_by-David-Revoy', '', $option_get);
$picture_date = substr($picture_date, 0, 10);

# Build an array with all pictures from the folder for navigation next/prev buttons.
$all_pictures = glob($sources.'/0ther/'.$content.'/low-res/*.jpg');
sort($all_pictures);
$picture_index = array();
foreach ($all_pictures as $key => $picture) {
  $picture = basename($picture);
  $picture = str_replace('.jpg', '', $picture);
  array_push($picture_index, $picture);
}
sort($picture_index);

# Guess where the high resolution path
# pattern by default
$picture_hires = $sources.'/0ther/'.$content.'/hi-res/'.$option_get.'.jpg';
if(!file_exists($picture_hires)){
  # if default not found, check if hi-res is a png
  $picture_hires = $sources.'/0ther/'.$content.'/hi-res/'.$option_get.'.png';
  if(!file_exists($picture_hires)){
    # if png not found, try png without attribution.
    $picture_hires = str_replace('_by-David-Revoy', '', $picture_hires);
  }
}
# Extract dimension and size on disk
if(file_exists($picture_hires)){
  $image_size = getimagesize($picture_hires);
  $fileweight = '('.round((filesize($picture_hires) / 1024) / 1024, 2).'MB)';
}

if ($content == "artworks" OR $content == "sketchbook" OR $content == "misc") {
  $previous_gallery = "artworks";
} else if ($content == "fan-art") {
  $previous_gallery = "fan-art";
} else {
  $previous_gallery = "files";
}

# Viewer mode
# -----------

  # Container for the header (max-width to group center the buttons)
  echo '<div class="viewheader">'."\n";

  # Button to toggle switch Source and License
  echo '  <a class="viewdownload'.$source_button_class.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content_for_url.'__'.$option_get.'.html">'."\n";
  echo '    '._("source and license").''."\n";
  echo '  </a>'."\n";

  # Main Title + attribution
  echo '  <span class="viewheadertitle">'."\n";
  echo '    '.$title.''."\n";
  echo '  </span>&nbsp;'."\n";
  echo '  <span class="viewheaderattribution">'."\n";
  echo '    '._("by").' '.$attribution.''."\n";
  echo '  </span>&nbsp;'."\n";
  echo '  <br/>';

  # Close button
  echo '  <a class="viewcloseallbutton" title="'._("Return to the gallery of thumbnails.").'" href="'.$root.'/'.$lang.'/'.$previous_gallery.'/'.$content.'.html#'.$option_get.'">'."\n";
  echo '    <img src="'.$root.'/core/img/close.svg"/>';
  echo '  </a>'."\n";

  echo ''._navigation($option_get, $picture_index, $mode).'';

  echo '  </div>'."\n";

echo '<div style="clear:both;"></div>'."\n";

  # Container for both image and sources panel
  echo '<div class="viewimagecontainer">'."\n";

  # Image (big)
  # -----------
  $picture_description = $title.' '._("by").' '.$attribution.'';
  echo '  <a href="'.$root.'/'.$picture_hires.'">'."\n";
  echo '    <img title="'.$picture_description.'" src="'.$root.'/'.$pattern.'"/>';
  echo '  </a>'."\n";

  if ($viewer_mode == 'show-sources') {
    # Sources panel:
    # --------------
    # Container
    echo '  <div id="viewsrccontainer">'."\n";

    # Close button
    echo '  <a class="viewsrcclosebutton" title="'._("Return to the gallery of thumbnails.").'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content_for_url.'__'.$option_get.'.html">'."\n";
    echo '    <img src="'.$root.'/core/img/close_mini.svg"/>';
    echo '  </a>'."\n";

    # Title
    echo '    <h1>'.$title.'</h1><br/>';
    # Thumbnail
    echo '<div class="viewsrcthumb">'."\n";
    _img(''.$root.'/'.$pattern, $picture_description, 560, 250, 87);
    echo '</div>'."\n";
    echo '  <br/>';
    echo '  <br/>';
    # Image infos
    # Make a better category label
    $category = $content;
    # Check if we have a i18n label for it
    if(isset($other_directories[$content])) {
      $category = $other_directories[$content];
    }
    echo '  '._("Artist:").' <strong>'.$attribution.'</strong><br/>';
    echo '  '._("Original size:").' '.$image_size[0].'x'.$image_size[1].'px <br/>';
    echo '  '._("Created on:").' '.$picture_date.'<br/>';
    echo '  '._("Category:").' '.$category.'<br/>';
    echo '  <br/>';

    # Links to files
    if(file_exists($picture_hires)){
      $image_size = getimagesize($picture_hires);
      $fileweight = '('.round((filesize($picture_hires) / 1024) / 1024, 2).'MB)';
      echo '  <a class="viewsrcdownloadbutton" style="width:80%" href="'.$root.'/'.$picture_hires.'">'."\n";
      echo '  '._("Download the high resolution picture").'';
      echo '  </a><br/>'."\n";
    }
    $source_zip_file = $sources.'/0ther/'.$content.'/zip/'.$option_get.'.zip';
    if(file_exists($source_zip_file)){
      $fileweight = '('.round((filesize($source_zip_file) / 1024) / 1024, 2).'MB)';
      echo '  <a class="viewsrcdownloadbutton" style="width:80%" href="'.$root.'/'.$source_zip_file.'">'."\n";
      echo '  '._("Download the source file (zip)").'';
      echo '  </a>'."\n";
    }

    # License
    $goodpracticelink = ''.$root.'/'.$lang.'/documentation/120_License_best_practices.html';
    echo '  <br/>';
    echo '  <br/>';
    echo '<div class="viewsrclicensebox">';
    echo '<strong>'._("License:").'</strong><br/>';
    if ( $content !== 'fan-art' ){
      echo '<img src="'.$root.'/core/img/ccby.jpg" style="margin-top: 10px;"/><br/>';
      echo '<br/><a href="https://creativecommons.org/licenses/by/4.0/">'._("Creative Commons Attribution 4.0 International license").'</a><br/>';
      echo ''._("Attribution to").' <strong>'.$attribution.'</strong><br/><br/>';
      echo ''.sprintf(_("More information and good practice for attribution can be found <a href=\"%s\">on the documentation</a>."),$goodpracticelink).'<br/>';
    } else {
      # fan-art license exception
      echo '<br/>'.sprintf(_("This picture is fan-art made by %s. It is reposted on the fan-art gallery of Pepper&Carrot with permission."),$attribution).'<br/>';
      echo '<br/>'._("Do not reuse this picture for your project unless you obtain author's permissions.").'';
    }
    # TODO: Add a link to documentation
    echo '</div>';

    echo '</div>';
  }

echo '  </div>'."\n";
echo '  </div>'."\n";
?>
